README
======

This directory contains a set of examples to familiarize yourself with different modeling setups and for testing purposes.

Each example requires a `Par_file`, `SOURCE`, and either a file of interfaces OR an external mesh. 
The examples can be run in the current example directory, using the processing script:
```
./run_this_example.sh
```

If you're interested in how to setup and run a simple SPECFEM2D simulation, 
we suggest to look at the **simple_topography_and_also_a_simple_fluid_layer/** example.

Please **consider submitting your own example** to this package!

